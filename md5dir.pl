#!/usr/bin/perl
#===============================================================================
#
#         FILE:  md5dir.pl
#
#        USAGE: md5dir.pl dir1 [dir2 ... dirn]
#
#  DESCRIPTION: Get md5 hash for a directory (or more).
#
# REQUIREMENTS:
#
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  Nelo R. Tovar tovar.nelo at gmail.com
#      COMPANY:  ---
#      VERSION:  1.00
#      CREATED:  11/11/2013
#      REVISION: Date         Autor                Change
#               27/11/2013  Nelo R. Tovar       Set permissions to 755
#===============================================================================
use strict;
use warnings;
use v5.10.1;
use Digest::MD5::File qw(dir_md5_hex file_md5_hex url_md5_hex);

$| = 1;
my @directories = @ARGV;
foreach my $dir (@directories) {
	if ( -d $dir ) {
		my $md5 = Digest::MD5->new;
		$md5->adddir($dir);
		my $digest = $md5->hexdigest;
		if ($!) {
			warn "Error on $dir: $!";
		} else {
			say "$dir=> $digest";
		};    # End of if ;
	} else {
		say "$dir is not a directory";
	} ;        # End of if
};    # End of foreach dir (@directories
__END__
=head1 NAME

md5dir.pl - Utility for getting MD5 sum for one or more directories

=head1 VERSION

Version 1.0

=head1 SYNOPSIS

md5dir.pl dir1 [dir2 .. dirn]

=head1 AUTHOR

Nelo R. Tovar, C<< <tovar.nelo at gmail.com> >>

=head1 BUGS

Nothing for the moments

=head1 SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc md5dir.pl

=head1 LICENSE AND COPYRIGHT

Copyright 2013 Nelo R. Tovar, tovar.nelo at g m a i l.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

L<http://www.perlfoundation.org/artistic_license_2_0>

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

=cut
